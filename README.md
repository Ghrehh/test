# Pizzabot

My solution for the coding challenge was written in Ruby. It can be executed in
the following ways:


## 1. Running the Example File

- Navigate to the `example` directory.

- Install bundler `gem install bundler`

- Install Pizzabot, which is required in the Gemfile `bundle install`

- Run with `bundle exec ruby example.rb '10x10 (3,3)'`, replacing the grid and
  locations parameters as you see fit.


## 2. Install the gem

- In the root directory, install bundler `gem install bundler`

- Install the gems in the Gemfile `bundle install`

- Build the gem locally `rake build`

- Install the Pizzabot gem `gem install pkg/pizzabot-0.1.0.gem`

- Run from the command line like so `pizzabot '3x3 (1,1) (2,0)'`


# General Information

My solution consists of three main parts:

- An `InputParser` module, which deals with converting the inputted string into
  formats more easily manipulated.

- A `Validator` class, which ensures the provided parameters are valid.

- A `DirectionsPlanner` class, which builds the instruction string based on the
  input string.

The above components are then used in tandem in the `create_instructions`
method.


# Error Handling

I made some effort to handle errors relating to invalid grid sizes and invalid
locations.


# Testing

I included both unit tests for all the above classes/modules, as well as an
integration test in `spec/pizzabot_spec.rb`.

Tests can be ran by:

- In the root directory, install bundler `gem install bundler`

- Install the gems in the Gemfile `bundle install`

- Run the tests with `rake`
