# frozen_string_literal: true

module Pizzabot
  module InputParser
    LOCATIONS_DELIMITER = ','
    GRID_DELIMITER = 'x'
    LOCATIONS_CHARACTERS_TO_DISCARD = '()'

    private_constant :LOCATIONS_DELIMITER,
                     :GRID_DELIMITER,
                     :LOCATIONS_CHARACTERS_TO_DISCARD

    def self.parse_grid_parameters(input)
      grid_parameters_string = input.split.first
      grid_parameters_array = grid_parameters_string.split(GRID_DELIMITER)

      {
        width: Integer(grid_parameters_array.first),
        height: Integer(grid_parameters_array.last)
      }
    end

    def self.parse_delivery_locations(input)
      cleaned_input = input.delete(LOCATIONS_CHARACTERS_TO_DISCARD)
      location_strings = cleaned_input.split[1..-1]
      format(location_strings)
    end

    private_class_method def self.format(location_strings)
      location_strings.map do |location_string|
        location_parameters = location_string.split(LOCATIONS_DELIMITER)

        {
          longitude: Integer(location_parameters.first),
          latitude: Integer(location_parameters.last)
        }
      end
    end
  end
end
