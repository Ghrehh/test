# frozen_string_literal: true

module Pizzabot
  class DirectionsPlanner
    DEFAULT_LATITUDE = 0
    DEFAULT_LONGITUDE = 0
    DELIVERY = 'D'
    NORTH = 'N'
    EAST = 'E'
    SOUTH = 'S'
    WEST = 'W'

    private_constant :DEFAULT_LONGITUDE,
                     :DEFAULT_LATITUDE,
                     :DELIVERY,
                     :NORTH,
                     :EAST,
                     :SOUTH,
                     :WEST

    attr_reader :instructions

    def initialize
      @current_latitude = DEFAULT_LATITUDE
      @current_longitude = DEFAULT_LONGITUDE
      @instructions = ''
    end

    def deliver_to(location)
      until at_location?(location)
        move_north if @current_latitude < location[:latitude]
        move_east if @current_longitude < location[:longitude]
        move_south if @current_latitude > location[:latitude]
        move_west if @current_longitude > location[:longitude]
      end

      deliver_pizza
    end

    def deliver_pizza
      @instructions += DELIVERY
    end

    private

    def move_north
      @instructions += NORTH
      @current_latitude += 1
    end

    def move_east
      @instructions += EAST
      @current_longitude += 1
    end

    def move_south
      @instructions += SOUTH
      @current_latitude -= 1
    end

    def move_west
      @instructions += WEST
      @current_longitude -= 1
    end

    def at_location?(location)
      at_correct_latitude = (location[:latitude] == @current_latitude)
      at_correct_longitude = (location[:longitude] == @current_longitude)

      at_correct_latitude && at_correct_longitude
    end
  end
end
