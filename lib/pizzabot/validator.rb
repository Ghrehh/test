module Pizzabot
  class Validator
    def initialize(grid:, delivery_locations:, directions_planner:)
      raise GridError unless valid_grid?(grid)
      @grid = grid
      @delivery_locations = delivery_locations
      @directions_planner = directions_planner
    end

    def run
      @delivery_locations.each do |location|
        raise LocationError unless valid_location?(location)
        @directions_planner.deliver_to(location)
      end

      @directions_planner.instructions
    end

    private

    def valid_grid?(grid)
      grid[:height] > 0 && grid[:width] > 0
    end

    def valid_location?(location)
      latitude = location[:latitude]
      longitude = location[:longitude]

      valid_latitude?(latitude) && valid_longitude?(longitude)
    end

    def valid_latitude?(latitude)
      latitude < @grid[:height] && latitude >= 0
    end

    def valid_longitude?(longitude)
      longitude < @grid[:width] && longitude >= 0
    end
  end
end
