# frozen_string_literal: true

module Pizzabot
  class LocationError < StandardError
    def initialize(msg = 'Invalid Location Provided')
      super(msg)
    end
  end

  class GridError < StandardError
    def initialize(msg = 'Invalid Grid Size Provided')
      super(msg)
    end
  end
end
