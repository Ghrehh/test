# frozen_string_literal: true


require 'pizzabot/version'
require 'pizzabot/exceptions'
require 'pizzabot/input_parser'
require 'pizzabot/directions_planner'
require 'pizzabot/validator'

module Pizzabot
  def self.create_instructions(input)
    Pizzabot::Validator.new(
      grid: Pizzabot::InputParser.parse_grid_parameters(input),
      delivery_locations: Pizzabot::InputParser.parse_delivery_locations(input),
      directions_planner: Pizzabot::DirectionsPlanner.new
    ).run
  rescue StandardError => error
    error.message
  end
end
