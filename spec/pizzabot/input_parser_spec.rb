# frozen_string_literal: true

RSpec.describe Pizzabot::InputParser do
  let(:input) { '3x4 (0,0) (1,1) (3,4)' }

  describe '#parse_grid_parameters' do
    subject(:parser) { described_class.parse_grid_parameters(input) }

    let(:expected_output) do
      {
        width: 3,
        height: 4
      }
    end

    it { expect(parser).to eq(expected_output) }
  end

  describe '#parse_delivery_locations' do
    subject(:parser) { described_class.parse_delivery_locations(input) }

    let(:expected_output) do
      [
        {
          longitude: 0,
          latitude: 0
        },
        {
          longitude: 1,
          latitude: 1
        },
        {
          longitude: 3,
          latitude: 4
        }
      ]
    end

    it { expect(parser).to eq(expected_output) }
  end
end
