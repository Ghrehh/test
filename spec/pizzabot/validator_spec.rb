# frozen_string_literal: true

RSpec.describe Pizzabot::Validator do
  describe '#run' do
    subject(:validator) do
      Pizzabot::Validator.new(
        grid: grid,
        delivery_locations: delivery_locations,
        directions_planner: directions_planner
      ).run
    end

    let(:grid) do
      {
        height: 5,
        width: 5
      }
    end

    let(:directions_planner) do
      instance_double(
        Pizzabot::DirectionsPlanner,
        instructions: 'some instructions'
      )
    end

    let(:delivery_locations) do
      [
        {
          latitude: 4,
          longitude: 4
        },
        {
          latitude: 3,
          longitude: 2
        },
        {
          latitude: 0,
          longitude: 1
        }
      ]
    end

    context 'with valid locations' do
      before do
        allow(directions_planner).to receive(:deliver_to).with(
          delivery_locations[0]
        )

        allow(directions_planner).to receive(:deliver_to).with(
          delivery_locations[1]
        )

        allow(directions_planner).to receive(:deliver_to).with(
          delivery_locations[2]
        )
      end

      it { expect(validator).to eq('some instructions') }
    end

    context 'invalid location' do
      let(:error) { Pizzabot::LocationError }

      context 'negative coordinates' do
        let(:delivery_locations) { [{ latitude: -4, longitude: -4 }] }
        it { expect { validator }.to raise_error(error) }
      end

      context 'coordinates too large for provided grid' do
        let(:delivery_locations) { [{ latitude: 4, longitude: 6 }] }
        it { expect { validator }.to raise_error(error) }
      end
    end

    context 'invalid grid' do
      let(:error) { Pizzabot::GridError }

      let(:grid) do
        {
          height: -1,
          width: 0
        }
      end

      it { expect { validator }.to raise_error(error) }
    end
  end
end
