# frozen_string_literal: true

RSpec.describe Pizzabot::DirectionsPlanner do
  describe 'default instructions' do
    subject(:instructions) { described_class.new.instructions }

    it { expect(instructions).to eq('') }
  end

  describe '#deliver_to' do
    subject(:planner) { described_class.new }

    context 'location is at default position' do
      let(:location) do
        {
          latitude: 0,
          longitude: 0
        }
      end

      before do
        planner.deliver_to(location)
      end

      it { expect(planner.instructions).to eq('D') }
    end

    context 'location is north' do
      let(:location) do
        {
          latitude: 1,
          longitude: 0
        }
      end

      before do
        planner.deliver_to(location)
      end

      it { expect(planner.instructions).to eq('ND') }
    end

    context 'location is east' do
      let(:location) do
        {
          latitude: 0,
          longitude: 1
        }
      end

      before do
        planner.deliver_to(location)
      end

      it { expect(planner.instructions).to eq('ED') }
    end

    context 'location is north east' do
      let(:location) do
        {
          latitude: 3,
          longitude: 1
        }
      end

      before do
        planner.deliver_to(location)
      end

      it { expect(planner.instructions).to eq('NENND') }
    end

    context 'locations are north east then south west' do
      let(:first_location) do
        {
          latitude: 3,
          longitude: 3
        }
      end

      let(:second_location) do
        {
          latitude: 0,
          longitude: 1
        }
      end

      before do
        planner.deliver_to(first_location)
        planner.deliver_to(second_location)
      end

      it { expect(planner.instructions).to eq('NENENEDSWSWSD') }
    end
  end
end
