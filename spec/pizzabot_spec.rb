# frozen_string_literal: true

RSpec.describe Pizzabot do
  it 'has a version number' do
    expect(Pizzabot::VERSION).not_to be nil
  end

  describe '.create_instructions' do
    {
      '1x1 (0,0)' => 'D',
      '5x5 (1,3) (4,4)' => 'NENNDNEEED',
      '5x5 (0,0) (1,3) (4,4) (4,2) (4,2) (0,1) (3,2) (2,3) (4,1)' => 'DNENNDNEEEDSSDDSWWWWDNEEEDNWDESESD',
      '0x-1' => 'Invalid Grid Size Provided',
      '1x1 (100,100)' => 'Invalid Location Provided'
    }.each do |input, expected_output|
      it { expect(Pizzabot.create_instructions(input)).to eq(expected_output) }
    end
  end
end
